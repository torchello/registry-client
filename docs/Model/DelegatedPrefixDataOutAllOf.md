# # DelegatedPrefixDataOutAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delegator_mo** | [**\OpenAPI\Client\Model\MO**](MO.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
