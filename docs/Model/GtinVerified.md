# # GtinVerified

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | **string** |  |
**gpc_category_code** | **string** |  |
**brand_name** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  | [optional]
**product_description** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  |
**product_image_url** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  | [optional]
**net_content** | [**\OpenAPI\Client\Model\NetContent[]**](NetContent.md) |  | [optional]
**country_of_sale_code** | [**\OpenAPI\Client\Model\CountryOfSaleCode[]**](CountryOfSaleCode.md) |  | [optional]
**is_complete** | **bool** |  |
**date_updated** | **\DateTime** |  |
**gtin_record_status** | **string** |  | [optional]
**licensee_name** | **string** |  |
**linkset** | [**\OpenAPI\Client\Model\Linkset**](Linkset.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
