# # GtinDataInAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin_status** | [**\OpenAPI\Client\Model\Status**](Status.md) |  |
**country_of_sale_code** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
