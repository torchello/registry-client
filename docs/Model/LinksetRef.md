# # LinksetRef

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**linkset** | [**\OpenAPI\Client\Model\Linkset**](Linkset.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
