# # LicenceDerivedKeyOneOf1

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gcp** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
