# # GtinDataOutAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_of_sale_code** | [**\OpenAPI\Client\Model\CountryOfSaleCode[]**](CountryOfSaleCode.md) |  | [optional]
**is_complete** | **bool** |  |
**date_updated** | **\DateTime** |  |
**gtin_record_status** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
