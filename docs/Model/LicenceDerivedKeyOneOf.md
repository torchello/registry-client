# # LicenceDerivedKeyOneOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | [**\OpenAPI\Client\Model\GTIN**](GTIN.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
