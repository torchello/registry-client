# # LicenceKeyAndType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licence_key** | [**OneOfGTINStringString**](OneOfGTINStringString.md) |  |
**licence_type** | [**\OpenAPI\Client\Model\LicenceType**](LicenceType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
