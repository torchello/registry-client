# # Gtin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | **string** |  |
**gpc_category_code** | **string** |  |
**brand_name** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  | [optional]
**product_description** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  |
**product_image_url** | [**\OpenAPI\Client\Model\LanguageBasedAttribute[]**](LanguageBasedAttribute.md) |  | [optional]
**net_content** | [**\OpenAPI\Client\Model\NetContent[]**](NetContent.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
