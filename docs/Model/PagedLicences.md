# # PagedLicences

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | [**\OpenAPI\Client\Model\PageMetadata**](PageMetadata.md) |  |
**_links** | [**\OpenAPI\Client\Model\PageLinks**](PageLinks.md) |  |
**_embedded** | [**\OpenAPI\Client\Model\PagedLicencesEmbedded**](PagedLicencesEmbedded.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
