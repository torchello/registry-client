# # GtinVerifiedWithLicenceAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**date_created** | **\DateTime** |  |
**gs1_licence** | [**LicenceKeyAndType**](LicenceKeyAndType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
