# # CountryOfSaleCode

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**numeric** | **string** |  |
**alpha2** | **string** |  | [optional]
**alpha3** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
