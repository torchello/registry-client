# # PagedLicencesEmbedded

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licences** | [**\OpenAPI\Client\Model\LicenceDataOut[]**](LicenceDataOut.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
