# # Link

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**href** | **string** |  |
**title** | **string** |  | [optional]
**context** | **string** |  | [optional]
**type** | **string** | Refer https://www.iana.org/assignments/media-types/media-types.xhtml | [optional]
**hreflang** | **string[]** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
