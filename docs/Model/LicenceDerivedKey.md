# # LicenceDerivedKey

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | [**\OpenAPI\Client\Model\GTIN**](GTIN.md) |  |
**gcp** | **string** |  |
**gln** | **string** |  |
**giai** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
