# # PagedDelegatedPrefixesEmbedded

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delegated_prefixes** | [**\OpenAPI\Client\Model\DelegatedPrefixDataOut[]**](DelegatedPrefixDataOut.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
