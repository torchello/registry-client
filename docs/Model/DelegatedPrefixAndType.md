# # DelegatedPrefixAndType

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delegated_prefix** | [**OneOfStringString**](OneOfStringString.md) |  |
**delegated_prefix_type** | [**\OpenAPI\Client\Model\LicenceType**](LicenceType.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
