# # GtinValidAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
