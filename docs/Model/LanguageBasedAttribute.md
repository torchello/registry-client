# # LanguageBasedAttribute

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**language** | **string** |  |
**value** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
