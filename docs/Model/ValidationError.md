# # ValidationError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**property** | **string** |  |
**index** | **int** | Indicates element position of invalid field if propery is an array | [optional]
**errors** | [**\OpenAPI\Client\Model\ErrorResponse[]**](ErrorResponse.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
