# # DelegatedPrefixDataOut

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delegated_prefix** | [**OneOfStringString**](OneOfStringString.md) |  |
**delegated_prefix_type** | [**\OpenAPI\Client\Model\LicenceType**](LicenceType.md) |  |
**delegatee_mo** | [**\OpenAPI\Client\Model\MO**](MO.md) |  |
**date_issued** | [**OneOfDateDateTime**](OneOfDateDateTime.md) |  | [optional]
**delegator_mo** | [**\OpenAPI\Client\Model\MO**](MO.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
