# # MO

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mo_name** | **string** |  | [optional]
**mo_gln** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
