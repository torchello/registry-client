# # DelegatedPrefixAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**delegatee_mo** | [**\OpenAPI\Client\Model\MO**](MO.md) |  |
**date_issued** | [**OneOfDateDateTime**](OneOfDateDateTime.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
