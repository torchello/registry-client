# # LicenceAllOf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**licence_status** | [**\OpenAPI\Client\Model\Status**](Status.md) |  |
**licensee_gln** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
