# # Linkset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**anchor** | **string** |  |
**item_description** | **string** |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
