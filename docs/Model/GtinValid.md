# # GtinValid

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gtin** | **string** |  |
**licensee_name** | **string** |  |
**linkset** | [**\OpenAPI\Client\Model\Linkset**](Linkset.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
