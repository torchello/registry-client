# # PageLinks

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first** | [**\OpenAPI\Client\Model\PageLink**](PageLink.md) |  |
**prev** | [**\OpenAPI\Client\Model\PageLink**](PageLink.md) |  | [optional]
**self** | [**\OpenAPI\Client\Model\PageLink**](PageLink.md) |  |
**next** | [**\OpenAPI\Client\Model\PageLink**](PageLink.md) |  | [optional]
**last** | [**\OpenAPI\Client\Model\PageLink**](PageLink.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
