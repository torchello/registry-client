# OpenAPI\Client\RoleGRPv3DelegateInApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPrefixDelegates()**](RoleGRPv3DelegateInApi.md#createPrefixDelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
[**queryBatchById()**](RoleGRPv3DelegateInApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `createPrefixDelegates()`

```php
createPrefixDelegates($delegated_prefix): string
```

Create and assign Delegated Prefixes

Data In - Create and assign delegated prefixes to other MOs;  delegated prefixes must be derived from the authorising MO's GS1 Prefix range. This endpoint is only available to MOs who have contractually delegated prefixes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoleGRPv3DelegateInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delegated_prefix = [{"delegatedPrefix":"12345","delegatedPrefixType":"GCP","dateIssued":"2012-04-23","delegateeMO":{"moGLN":"123467890127"}}]; // \OpenAPI\Client\Model\DelegatedPrefix[] | prefixes

try {
    $result = $apiInstance->createPrefixDelegates($delegated_prefix);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoleGRPv3DelegateInApi->createPrefixDelegates: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegated_prefix** | [**\OpenAPI\Client\Model\DelegatedPrefix[]**](../Model/DelegatedPrefix.md)| prefixes | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoleGRPv3DelegateInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoleGRPv3DelegateInApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
