# OpenAPI\Client\RoleGRPv3LicenceInfoApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getLicencesFromDerivedKeys()**](RoleGRPv3LicenceInfoApi.md#getLicencesFromDerivedKeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys


## `getLicencesFromDerivedKeys()`

```php
getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string): \OpenAPI\Client\Model\LicenceInfo[]
```

Get Licences from derived GS1 Keys

Data Out - Get Licences in the GRP from derived GS1 Keys. This capability is meant to support internal MO processes. The data should not be made publically available. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoleGRPv3LicenceInfoApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key_type = 'key_type_example'; // string | GS1 Key Type
$one_of_gtin_string_string_string = ["03014260091484"]; // OneOfGTINStringStringString[] | keys

try {
    $result = $apiInstance->getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoleGRPv3LicenceInfoApi->getLicencesFromDerivedKeys: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_type** | **string**| GS1 Key Type |
 **one_of_gtin_string_string_string** | [**OneOfGTINStringStringString[]**](../Model/OneOfGTINStringStringString.md)| keys | [optional]

### Return type

[**\OpenAPI\Client\Model\LicenceInfo[]**](../Model/LicenceInfo.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
