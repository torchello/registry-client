# OpenAPI\Client\LicencesApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllLicencesPaged()**](LicencesApi.md#getAllLicencesPaged) | **GET** /licences | Get ALL My Licences Paged


## `getAllLicencesPaged()`

```php
getAllLicencesPaged($page, $size): \OpenAPI\Client\Model\PagedLicences
```

Get ALL My Licences Paged

Data Out - Get All Licences in the GRP that are associated with my MO, page by page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\LicencesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | Format - int32. page
$size = 56; // int | Format - int32. size

try {
    $result = $apiInstance->getAllLicencesPaged($page, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicencesApi->getAllLicencesPaged: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| Format - int32. page | [optional]
 **size** | **int**| Format - int32. size | [optional]

### Return type

[**\OpenAPI\Client\Model\PagedLicences**](../Model/PagedLicences.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/hal+json`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
