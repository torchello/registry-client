# OpenAPI\Client\RoleGRPv3GtinOutApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getVerifiedGtins()**](RoleGRPv3GtinOutApi.md#getVerifiedGtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
[**getVerifiedGtinsWithLicence()**](RoleGRPv3GtinOutApi.md#getVerifiedGtinsWithLicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)


## `getVerifiedGtins()`

```php
getVerifiedGtins($request_body): OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]
```

Get Verified GTINs (Public)

Data Out - Get Verified GTINs with Licensee Name. This data can be a pass-through from your VbG service. The entire output can be shared with your members as-is, and is therefore considered \"public\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoleGRPv3GtinOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569","1234567890123"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtins($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoleGRPv3GtinOutApi->getVerifiedGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]**](../Model/OneOfGtinVerifiedGtinValidFeedbackErrorResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVerifiedGtinsWithLicence()`

```php
getVerifiedGtinsWithLicence($request_body): \OpenAPI\Client\Model\GtinVerifiedWithLicence[]
```

Get Verified GTINs With Licence (Private)

Data Out - Get Verified GTINs with additional license information FOR MO USE ONLY! This data CANNOT be a passed-through as-is from your VbG service. It contains additional license information (key, type, Licensing and Primary MO name) to support internal MO business processes and is therefore considered \"PRIVATE\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\RoleGRPv3GtinOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtinsWithLicence($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RoleGRPv3GtinOutApi->getVerifiedGtinsWithLicence: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**\OpenAPI\Client\Model\GtinVerifiedWithLicence[]**](../Model/GtinVerifiedWithLicence.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
