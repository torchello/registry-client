# OpenAPI\Client\LicenceApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrUpdateLicences()**](LicenceApi.md#createOrUpdateLicences) | **POST** /licences | Create or Update Licences OR Post Licence No Change Log
[**deleteLicencesByKeyAndType()**](LicenceApi.md#deleteLicencesByKeyAndType) | **DELETE** /licences | Delete Licences
[**getLicencesFromDerivedKeys()**](LicenceApi.md#getLicencesFromDerivedKeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys
[**queryBatchById()**](LicenceApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `createOrUpdateLicences()`

```php
createOrUpdateLicences($licence): string
```

Create or Update Licences OR Post Licence No Change Log

Data In - Create and/or Update new and existing Licences in the GRP; each Licence must be derived from a GS1 and/or delegated prefix assigned to the authorising MO; if an empty batch is sent, then a Licence \"No Change\" event will be logged in the Licence Data Quality dashboard.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\LicenceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$licence = [{"licenceKey":"301426","licenceType":"GCP","licenseeName":"PROCTER ET GAMBLE FRANCE SAS","licenseeGLN":"3010248700102","licenceStatus":"ACTIVE"}]; // \OpenAPI\Client\Model\Licence[] | licences

try {
    $result = $apiInstance->createOrUpdateLicences($licence);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenceApi->createOrUpdateLicences: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licence** | [**\OpenAPI\Client\Model\Licence[]**](../Model/Licence.md)| licences | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteLicencesByKeyAndType()`

```php
deleteLicencesByKeyAndType($licence_key_and_type): string
```

Delete Licences

Data In - Delete existing Licences in the GRP; Each Licence must exist and be assigned to authorising MO. All GTINs under the target licence must be deleted from the GRP before the licence can be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\LicenceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$licence_key_and_type = [{"licenceKey":"301426","licenceType":"GCP"}]; // \OpenAPI\Client\Model\LicenceKeyAndType[] | licences

try {
    $result = $apiInstance->deleteLicencesByKeyAndType($licence_key_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenceApi->deleteLicencesByKeyAndType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licence_key_and_type** | [**\OpenAPI\Client\Model\LicenceKeyAndType[]**](../Model/LicenceKeyAndType.md)| licences | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getLicencesFromDerivedKeys()`

```php
getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string): \OpenAPI\Client\Model\LicenceInfo[]
```

Get Licences from derived GS1 Keys

Data Out - Get Licences in the GRP from derived GS1 Keys. This capability is meant to support internal MO processes. The data should not be made publically available. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\LicenceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key_type = 'key_type_example'; // string | GS1 Key Type
$one_of_gtin_string_string_string = ["03014260091484"]; // OneOfGTINStringStringString[] | keys

try {
    $result = $apiInstance->getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenceApi->getLicencesFromDerivedKeys: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_type** | **string**| GS1 Key Type |
 **one_of_gtin_string_string_string** | [**OneOfGTINStringStringString[]**](../Model/OneOfGTINStringStringString.md)| keys | [optional]

### Return type

[**\OpenAPI\Client\Model\LicenceInfo[]**](../Model/LicenceInfo.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\LicenceApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling LicenceApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
