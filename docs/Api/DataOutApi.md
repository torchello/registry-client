# OpenAPI\Client\DataOutApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**getAllDelegatedPrefixesPaged()**](DataOutApi.md#getAllDelegatedPrefixesPaged) | **GET** /prefix/delegates | Get All Assigned Delegated Prefixes Paged
[**getAllLicencesPaged()**](DataOutApi.md#getAllLicencesPaged) | **GET** /licences | Get ALL My Licences Paged
[**getLicencesFromDerivedKeys()**](DataOutApi.md#getLicencesFromDerivedKeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys
[**getVerifiedGtins()**](DataOutApi.md#getVerifiedGtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
[**getVerifiedGtinsWithLicence()**](DataOutApi.md#getVerifiedGtinsWithLicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)
[**queryBatchById()**](DataOutApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `getAllDelegatedPrefixesPaged()`

```php
getAllDelegatedPrefixesPaged($page, $size): \OpenAPI\Client\Model\PagedDelegatedPrefixes
```

Get All Assigned Delegated Prefixes Paged

Data Out - Get All Delegated Prefixes in the GRP currently assigned to my MO, page by page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | Format - int32. page
$size = 56; // int | Format - int32. size

try {
    $result = $apiInstance->getAllDelegatedPrefixesPaged($page, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->getAllDelegatedPrefixesPaged: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| Format - int32. page | [optional]
 **size** | **int**| Format - int32. size | [optional]

### Return type

[**\OpenAPI\Client\Model\PagedDelegatedPrefixes**](../Model/PagedDelegatedPrefixes.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/hal+json`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllLicencesPaged()`

```php
getAllLicencesPaged($page, $size): \OpenAPI\Client\Model\PagedLicences
```

Get ALL My Licences Paged

Data Out - Get All Licences in the GRP that are associated with my MO, page by page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | Format - int32. page
$size = 56; // int | Format - int32. size

try {
    $result = $apiInstance->getAllLicencesPaged($page, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->getAllLicencesPaged: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| Format - int32. page | [optional]
 **size** | **int**| Format - int32. size | [optional]

### Return type

[**\OpenAPI\Client\Model\PagedLicences**](../Model/PagedLicences.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/hal+json`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getLicencesFromDerivedKeys()`

```php
getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string): \OpenAPI\Client\Model\LicenceInfo[]
```

Get Licences from derived GS1 Keys

Data Out - Get Licences in the GRP from derived GS1 Keys. This capability is meant to support internal MO processes. The data should not be made publically available. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$key_type = 'key_type_example'; // string | GS1 Key Type
$one_of_gtin_string_string_string = ["03014260091484"]; // OneOfGTINStringStringString[] | keys

try {
    $result = $apiInstance->getLicencesFromDerivedKeys($key_type, $one_of_gtin_string_string_string);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->getLicencesFromDerivedKeys: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key_type** | **string**| GS1 Key Type |
 **one_of_gtin_string_string_string** | [**OneOfGTINStringStringString[]**](../Model/OneOfGTINStringStringString.md)| keys | [optional]

### Return type

[**\OpenAPI\Client\Model\LicenceInfo[]**](../Model/LicenceInfo.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVerifiedGtins()`

```php
getVerifiedGtins($request_body): OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]
```

Get Verified GTINs (Public)

Data Out - Get Verified GTINs with Licensee Name. This data can be a pass-through from your VbG service. The entire output can be shared with your members as-is, and is therefore considered \"public\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569","1234567890123"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtins($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->getVerifiedGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]**](../Model/OneOfGtinVerifiedGtinValidFeedbackErrorResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVerifiedGtinsWithLicence()`

```php
getVerifiedGtinsWithLicence($request_body): \OpenAPI\Client\Model\GtinVerifiedWithLicence[]
```

Get Verified GTINs With Licence (Private)

Data Out - Get Verified GTINs with additional license information FOR MO USE ONLY! This data CANNOT be a passed-through as-is from your VbG service. It contains additional license information (key, type, Licensing and Primary MO name) to support internal MO business processes and is therefore considered \"PRIVATE\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtinsWithLicence($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->getVerifiedGtinsWithLicence: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**\OpenAPI\Client\Model\GtinVerifiedWithLicence[]**](../Model/GtinVerifiedWithLicence.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataOutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataOutApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
