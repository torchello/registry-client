# OpenAPI\Client\GtinApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrModifyGtins()**](GtinApi.md#createOrModifyGtins) | **POST** /gtins | Create or Modify GTINs - With Licence
[**createOrModifyGtinsWithoutLicence()**](GtinApi.md#createOrModifyGtinsWithoutLicence) | **POST** /gtins/nolicence | Create or Modify GTINs - Without Licence
[**deleteGtins()**](GtinApi.md#deleteGtins) | **DELETE** /gtins | Delete GTINs
[**getVerifiedGtins()**](GtinApi.md#getVerifiedGtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
[**getVerifiedGtinsWithLicence()**](GtinApi.md#getVerifiedGtinsWithLicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)
[**queryBatchById()**](GtinApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `createOrModifyGtins()`

```php
createOrModifyGtins($gtin_with_licence_key_and_type): string
```

Create or Modify GTINs - With Licence

Data In - Create new and/or modify existing GTINs in the GRP; GTIN must be derived from an existing Licence in the GRP assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gtin_with_licence_key_and_type = [{"gtin":"03014261234569","licenceKey":"301426","licenceType":"GCP","gtinStatus":"ACTIVE","brandName":[{"language":"fr-FR","value":"Oral-B"}],"gpcCategoryCode":"10000383","countryOfSaleCode":["BE","FR"],"productDescription":[{"language":"fr-FR","value":"Oral-B Manual PRO-EXPERT DENTIFRICE DENTS SENSIBLES LOT 2X75ML 150ML"},{"language":"nl-BE","value":"Oral-B Manual PRO-EXPERT TANDPASTA Healthy White 150ML"},{"language":"fr-BE","value":"Oral-B Manual DENTIFRICE Healthy White 150ML"}],"productImageUrl":[{"language":"fr-FR","value":"https://delivery.pgbrandstore.com/api/public/content/fpc_81685484_master_preview"}],"netContent":[{"value":"150.0","unitCode":"MLT"}]}]; // \OpenAPI\Client\Model\GtinWithLicenceKeyAndType[] | gtins

try {
    $result = $apiInstance->createOrModifyGtins($gtin_with_licence_key_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->createOrModifyGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gtin_with_licence_key_and_type** | [**\OpenAPI\Client\Model\GtinWithLicenceKeyAndType[]**](../Model/GtinWithLicenceKeyAndType.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createOrModifyGtinsWithoutLicence()`

```php
createOrModifyGtinsWithoutLicence($gtin_data_in): string
```

Create or Modify GTINs - Without Licence

NOTE: DO NOT USE THIS ENDPOINT UNLESS YOUR SYSTEM CANNOT ALSO SEND THE CORRESPONDING LICENCE. ALL MOS SHOULD BE USING THE \"Create or Modify GTINs - With Licence\" ENDPOINT WHICH IS PERFORMANCE OPTIMISED. GS1 GO WILL TRACK USAGE OF THIS ENDPOINT TO ENSURE IT IS ONLY USED BY MOS WHO ARE INCAPABLE OF SENDING THE CORRESPONDING LICENCE. Data In - Create new and/or modify existing GTINs in the GRP; GTIN must be derived from an existing Licence in the GRP assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gtin_data_in = [{"gtin":"03014261234569","gtinStatus":"ACTIVE","brandName":[{"language":"fr-FR","value":"Oral-B"}],"gpcCategoryCode":"10000383","countryOfSaleCode":["BE","FR"],"productDescription":[{"language":"fr-FR","value":"Oral-B Manual PRO-EXPERT DENTIFRICE DENTS SENSIBLES LOT 2X75ML 150ML"},{"language":"nl-BE","value":"Oral-B Manual PRO-EXPERT TANDPASTA Healthy White 150ML"},{"language":"fr-BE","value":"Oral-B Manual DENTIFRICE Healthy White 150ML"}],"productImageUrl":[{"language":"fr-FR","value":"https://delivery.pgbrandstore.com/api/public/content/fpc_81685484_master_preview"}],"netContent":[{"value":"150.0","unitCode":"MLT"}]}]; // \OpenAPI\Client\Model\GtinDataIn[] | gtins

try {
    $result = $apiInstance->createOrModifyGtinsWithoutLicence($gtin_data_in);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->createOrModifyGtinsWithoutLicence: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gtin_data_in** | [**\OpenAPI\Client\Model\GtinDataIn[]**](../Model/GtinDataIn.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteGtins()`

```php
deleteGtins($request_body): string
```

Delete GTINs

Data In - Delete existing GTINs in the GRP; Each GTIN must be assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["00056100024798"]; // string[] | gtins

try {
    $result = $apiInstance->deleteGtins($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->deleteGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVerifiedGtins()`

```php
getVerifiedGtins($request_body): OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]
```

Get Verified GTINs (Public)

Data Out - Get Verified GTINs with Licensee Name. This data can be a pass-through from your VbG service. The entire output can be shared with your members as-is, and is therefore considered \"public\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569","1234567890123"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtins($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->getVerifiedGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**OneOfGtinVerifiedGtinValidFeedbackErrorResponse[]**](../Model/OneOfGtinVerifiedGtinValidFeedbackErrorResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getVerifiedGtinsWithLicence()`

```php
getVerifiedGtinsWithLicence($request_body): \OpenAPI\Client\Model\GtinVerifiedWithLicence[]
```

Get Verified GTINs With Licence (Private)

Data Out - Get Verified GTINs with additional license information FOR MO USE ONLY! This data CANNOT be a passed-through as-is from your VbG service. It contains additional license information (key, type, Licensing and Primary MO name) to support internal MO business processes and is therefore considered \"PRIVATE\". This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["03014261234569"]; // string[] | gtins

try {
    $result = $apiInstance->getVerifiedGtinsWithLicence($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->getVerifiedGtinsWithLicence: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

[**\OpenAPI\Client\Model\GtinVerifiedWithLicence[]**](../Model/GtinVerifiedWithLicence.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\GtinApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling GtinApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
