# OpenAPI\Client\DataInApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOrModifyGtins()**](DataInApi.md#createOrModifyGtins) | **POST** /gtins | Create or Modify GTINs - With Licence
[**createOrModifyGtinsWithoutLicence()**](DataInApi.md#createOrModifyGtinsWithoutLicence) | **POST** /gtins/nolicence | Create or Modify GTINs - Without Licence
[**createOrUpdateLicences()**](DataInApi.md#createOrUpdateLicences) | **POST** /licences | Create or Update Licences OR Post Licence No Change Log
[**createPrefixDelegates()**](DataInApi.md#createPrefixDelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
[**deleteDelegatedPrefixesByKeyAndType()**](DataInApi.md#deleteDelegatedPrefixesByKeyAndType) | **DELETE** /prefix/delegates | Delete Delegated Prefixes
[**deleteGtins()**](DataInApi.md#deleteGtins) | **DELETE** /gtins | Delete GTINs
[**deleteLicencesByKeyAndType()**](DataInApi.md#deleteLicencesByKeyAndType) | **DELETE** /licences | Delete Licences
[**queryBatchById()**](DataInApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `createOrModifyGtins()`

```php
createOrModifyGtins($gtin_with_licence_key_and_type): string
```

Create or Modify GTINs - With Licence

Data In - Create new and/or modify existing GTINs in the GRP; GTIN must be derived from an existing Licence in the GRP assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gtin_with_licence_key_and_type = [{"gtin":"03014261234569","licenceKey":"301426","licenceType":"GCP","gtinStatus":"ACTIVE","brandName":[{"language":"fr-FR","value":"Oral-B"}],"gpcCategoryCode":"10000383","countryOfSaleCode":["BE","FR"],"productDescription":[{"language":"fr-FR","value":"Oral-B Manual PRO-EXPERT DENTIFRICE DENTS SENSIBLES LOT 2X75ML 150ML"},{"language":"nl-BE","value":"Oral-B Manual PRO-EXPERT TANDPASTA Healthy White 150ML"},{"language":"fr-BE","value":"Oral-B Manual DENTIFRICE Healthy White 150ML"}],"productImageUrl":[{"language":"fr-FR","value":"https://delivery.pgbrandstore.com/api/public/content/fpc_81685484_master_preview"}],"netContent":[{"value":"150.0","unitCode":"MLT"}]}]; // \OpenAPI\Client\Model\GtinWithLicenceKeyAndType[] | gtins

try {
    $result = $apiInstance->createOrModifyGtins($gtin_with_licence_key_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->createOrModifyGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gtin_with_licence_key_and_type** | [**\OpenAPI\Client\Model\GtinWithLicenceKeyAndType[]**](../Model/GtinWithLicenceKeyAndType.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createOrModifyGtinsWithoutLicence()`

```php
createOrModifyGtinsWithoutLicence($gtin_data_in): string
```

Create or Modify GTINs - Without Licence

NOTE: DO NOT USE THIS ENDPOINT UNLESS YOUR SYSTEM CANNOT ALSO SEND THE CORRESPONDING LICENCE. ALL MOS SHOULD BE USING THE \"Create or Modify GTINs - With Licence\" ENDPOINT WHICH IS PERFORMANCE OPTIMISED. GS1 GO WILL TRACK USAGE OF THIS ENDPOINT TO ENSURE IT IS ONLY USED BY MOS WHO ARE INCAPABLE OF SENDING THE CORRESPONDING LICENCE. Data In - Create new and/or modify existing GTINs in the GRP; GTIN must be derived from an existing Licence in the GRP assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gtin_data_in = [{"gtin":"03014261234569","gtinStatus":"ACTIVE","brandName":[{"language":"fr-FR","value":"Oral-B"}],"gpcCategoryCode":"10000383","countryOfSaleCode":["BE","FR"],"productDescription":[{"language":"fr-FR","value":"Oral-B Manual PRO-EXPERT DENTIFRICE DENTS SENSIBLES LOT 2X75ML 150ML"},{"language":"nl-BE","value":"Oral-B Manual PRO-EXPERT TANDPASTA Healthy White 150ML"},{"language":"fr-BE","value":"Oral-B Manual DENTIFRICE Healthy White 150ML"}],"productImageUrl":[{"language":"fr-FR","value":"https://delivery.pgbrandstore.com/api/public/content/fpc_81685484_master_preview"}],"netContent":[{"value":"150.0","unitCode":"MLT"}]}]; // \OpenAPI\Client\Model\GtinDataIn[] | gtins

try {
    $result = $apiInstance->createOrModifyGtinsWithoutLicence($gtin_data_in);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->createOrModifyGtinsWithoutLicence: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gtin_data_in** | [**\OpenAPI\Client\Model\GtinDataIn[]**](../Model/GtinDataIn.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createOrUpdateLicences()`

```php
createOrUpdateLicences($licence): string
```

Create or Update Licences OR Post Licence No Change Log

Data In - Create and/or Update new and existing Licences in the GRP; each Licence must be derived from a GS1 and/or delegated prefix assigned to the authorising MO; if an empty batch is sent, then a Licence \"No Change\" event will be logged in the Licence Data Quality dashboard.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$licence = [{"licenceKey":"301426","licenceType":"GCP","licenseeName":"PROCTER ET GAMBLE FRANCE SAS","licenseeGLN":"3010248700102","licenceStatus":"ACTIVE"}]; // \OpenAPI\Client\Model\Licence[] | licences

try {
    $result = $apiInstance->createOrUpdateLicences($licence);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->createOrUpdateLicences: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licence** | [**\OpenAPI\Client\Model\Licence[]**](../Model/Licence.md)| licences | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createPrefixDelegates()`

```php
createPrefixDelegates($delegated_prefix): string
```

Create and assign Delegated Prefixes

Data In - Create and assign delegated prefixes to other MOs;  delegated prefixes must be derived from the authorising MO's GS1 Prefix range. This endpoint is only available to MOs who have contractually delegated prefixes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delegated_prefix = [{"delegatedPrefix":"12345","delegatedPrefixType":"GCP","dateIssued":"2012-04-23","delegateeMO":{"moGLN":"123467890127"}}]; // \OpenAPI\Client\Model\DelegatedPrefix[] | prefixes

try {
    $result = $apiInstance->createPrefixDelegates($delegated_prefix);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->createPrefixDelegates: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegated_prefix** | [**\OpenAPI\Client\Model\DelegatedPrefix[]**](../Model/DelegatedPrefix.md)| prefixes | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteDelegatedPrefixesByKeyAndType()`

```php
deleteDelegatedPrefixesByKeyAndType($delegated_prefix_and_type): string
```

Delete Delegated Prefixes

Data In - Delete existing Delegated Prefixes in the GRP; Each Delegated Prefix must exist and be delegated from the authorising MO. All Licences associated to the target delegated prefix must be deleted from the GRP before the delegated prefix can be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delegated_prefix_and_type = [{"licenceKey":"301426","licenceType":"GCP"}]; // \OpenAPI\Client\Model\DelegatedPrefixAndType[] | prefixes

try {
    $result = $apiInstance->deleteDelegatedPrefixesByKeyAndType($delegated_prefix_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->deleteDelegatedPrefixesByKeyAndType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegated_prefix_and_type** | [**\OpenAPI\Client\Model\DelegatedPrefixAndType[]**](../Model/DelegatedPrefixAndType.md)| prefixes | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteGtins()`

```php
deleteGtins($request_body): string
```

Delete GTINs

Data In - Delete existing GTINs in the GRP; Each GTIN must be assigned to the authorising MO. This endpoint is only available to MOs who are Activate-grade certified. Your API key will not work if you have not achieved this certification.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$request_body = ["00056100024798"]; // string[] | gtins

try {
    $result = $apiInstance->deleteGtins($request_body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->deleteGtins: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **request_body** | [**string[]**](../Model/string.md)| gtins | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteLicencesByKeyAndType()`

```php
deleteLicencesByKeyAndType($licence_key_and_type): string
```

Delete Licences

Data In - Delete existing Licences in the GRP; Each Licence must exist and be assigned to authorising MO. All GTINs under the target licence must be deleted from the GRP before the licence can be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$licence_key_and_type = [{"licenceKey":"301426","licenceType":"GCP"}]; // \OpenAPI\Client\Model\LicenceKeyAndType[] | licences

try {
    $result = $apiInstance->deleteLicencesByKeyAndType($licence_key_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->deleteLicencesByKeyAndType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **licence_key_and_type** | [**\OpenAPI\Client\Model\LicenceKeyAndType[]**](../Model/LicenceKeyAndType.md)| licences | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
