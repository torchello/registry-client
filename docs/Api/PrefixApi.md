# OpenAPI\Client\PrefixApi

All URIs are relative to https://grp.gs1.org/grp/v3.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createPrefixDelegates()**](PrefixApi.md#createPrefixDelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
[**deleteDelegatedPrefixesByKeyAndType()**](PrefixApi.md#deleteDelegatedPrefixesByKeyAndType) | **DELETE** /prefix/delegates | Delete Delegated Prefixes
[**getAllDelegatedPrefixesPaged()**](PrefixApi.md#getAllDelegatedPrefixesPaged) | **GET** /prefix/delegates | Get All Assigned Delegated Prefixes Paged
[**queryBatchById()**](PrefixApi.md#queryBatchById) | **GET** /feedback/{batchID} | Query batch status and response


## `createPrefixDelegates()`

```php
createPrefixDelegates($delegated_prefix): string
```

Create and assign Delegated Prefixes

Data In - Create and assign delegated prefixes to other MOs;  delegated prefixes must be derived from the authorising MO's GS1 Prefix range. This endpoint is only available to MOs who have contractually delegated prefixes.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PrefixApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delegated_prefix = [{"delegatedPrefix":"12345","delegatedPrefixType":"GCP","dateIssued":"2012-04-23","delegateeMO":{"moGLN":"123467890127"}}]; // \OpenAPI\Client\Model\DelegatedPrefix[] | prefixes

try {
    $result = $apiInstance->createPrefixDelegates($delegated_prefix);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrefixApi->createPrefixDelegates: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegated_prefix** | [**\OpenAPI\Client\Model\DelegatedPrefix[]**](../Model/DelegatedPrefix.md)| prefixes | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `deleteDelegatedPrefixesByKeyAndType()`

```php
deleteDelegatedPrefixesByKeyAndType($delegated_prefix_and_type): string
```

Delete Delegated Prefixes

Data In - Delete existing Delegated Prefixes in the GRP; Each Delegated Prefix must exist and be delegated from the authorising MO. All Licences associated to the target delegated prefix must be deleted from the GRP before the delegated prefix can be deleted.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PrefixApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$delegated_prefix_and_type = [{"licenceKey":"301426","licenceType":"GCP"}]; // \OpenAPI\Client\Model\DelegatedPrefixAndType[] | prefixes

try {
    $result = $apiInstance->deleteDelegatedPrefixesByKeyAndType($delegated_prefix_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrefixApi->deleteDelegatedPrefixesByKeyAndType: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegated_prefix_and_type** | [**\OpenAPI\Client\Model\DelegatedPrefixAndType[]**](../Model/DelegatedPrefixAndType.md)| prefixes | [optional]

### Return type

**string**

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `getAllDelegatedPrefixesPaged()`

```php
getAllDelegatedPrefixesPaged($page, $size): \OpenAPI\Client\Model\PagedDelegatedPrefixes
```

Get All Assigned Delegated Prefixes Paged

Data Out - Get All Delegated Prefixes in the GRP currently assigned to my MO, page by page

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PrefixApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = 56; // int | Format - int32. page
$size = 56; // int | Format - int32. size

try {
    $result = $apiInstance->getAllDelegatedPrefixesPaged($page, $size);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrefixApi->getAllDelegatedPrefixesPaged: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| Format - int32. page | [optional]
 **size** | **int**| Format - int32. size | [optional]

### Return type

[**\OpenAPI\Client\Model\PagedDelegatedPrefixes**](../Model/PagedDelegatedPrefixes.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/hal+json`, `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `queryBatchById()`

```php
queryBatchById($batch_id): \OpenAPI\Client\Model\FeedbackResponse[]
```

Query batch status and response

Data In/Out - Query Batch Status And Response; target batch must have been submitted by your MO.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\PrefixApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$batch_id = 'batch_id_example'; // string | Format - guid. Batch GUID from Data In Response

try {
    $result = $apiInstance->queryBatchById($batch_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PrefixApi->queryBatchById: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **batch_id** | **string**| Format - guid. Batch GUID from Data In Response |

### Return type

[**\OpenAPI\Client\Model\FeedbackResponse[]**](../Model/FeedbackResponse.md)

### Authorization

[apiKeyHeader](../../README.md#apiKeyHeader), [apiKeyQuery](../../README.md#apiKeyQuery)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
