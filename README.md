# OpenAPIClient-php

This is the high performance API that should only be used for production data coming from your internal systems and Activate-Grade tools


## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/GIT_REPO_ID.git"
    }
  ],
  "require": {
    "GIT_USER_ID/GIT_REPO_ID": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/OpenAPIClient-php/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure API key authorization: apiKeyHeader
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');

// Configure API key authorization: apiKeyQuery
$config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKey('APIKey', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = OpenAPI\Client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('APIKey', 'Bearer');


$apiInstance = new OpenAPI\Client\Api\DataInApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$gtin_with_licence_key_and_type = [{"gtin":"03014261234569","licenceKey":"301426","licenceType":"GCP","gtinStatus":"ACTIVE","brandName":[{"language":"fr-FR","value":"Oral-B"}],"gpcCategoryCode":"10000383","countryOfSaleCode":["BE","FR"],"productDescription":[{"language":"fr-FR","value":"Oral-B Manual PRO-EXPERT DENTIFRICE DENTS SENSIBLES LOT 2X75ML 150ML"},{"language":"nl-BE","value":"Oral-B Manual PRO-EXPERT TANDPASTA Healthy White 150ML"},{"language":"fr-BE","value":"Oral-B Manual DENTIFRICE Healthy White 150ML"}],"productImageUrl":[{"language":"fr-FR","value":"https://delivery.pgbrandstore.com/api/public/content/fpc_81685484_master_preview"}],"netContent":[{"value":"150.0","unitCode":"MLT"}]}]; // \OpenAPI\Client\Model\GtinWithLicenceKeyAndType[] | gtins

try {
    $result = $apiInstance->createOrModifyGtins($gtin_with_licence_key_and_type);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling DataInApi->createOrModifyGtins: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://grp.gs1.org/grp/v3*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*DataInApi* | [**createOrModifyGtins**](docs/Api/DataInApi.md#createormodifygtins) | **POST** /gtins | Create or Modify GTINs - With Licence
*DataInApi* | [**createOrModifyGtinsWithoutLicence**](docs/Api/DataInApi.md#createormodifygtinswithoutlicence) | **POST** /gtins/nolicence | Create or Modify GTINs - Without Licence
*DataInApi* | [**createOrUpdateLicences**](docs/Api/DataInApi.md#createorupdatelicences) | **POST** /licences | Create or Update Licences OR Post Licence No Change Log
*DataInApi* | [**createPrefixDelegates**](docs/Api/DataInApi.md#createprefixdelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
*DataInApi* | [**deleteDelegatedPrefixesByKeyAndType**](docs/Api/DataInApi.md#deletedelegatedprefixesbykeyandtype) | **DELETE** /prefix/delegates | Delete Delegated Prefixes
*DataInApi* | [**deleteGtins**](docs/Api/DataInApi.md#deletegtins) | **DELETE** /gtins | Delete GTINs
*DataInApi* | [**deleteLicencesByKeyAndType**](docs/Api/DataInApi.md#deletelicencesbykeyandtype) | **DELETE** /licences | Delete Licences
*DataInApi* | [**queryBatchById**](docs/Api/DataInApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*DataOutApi* | [**getAllDelegatedPrefixesPaged**](docs/Api/DataOutApi.md#getalldelegatedprefixespaged) | **GET** /prefix/delegates | Get All Assigned Delegated Prefixes Paged
*DataOutApi* | [**getAllLicencesPaged**](docs/Api/DataOutApi.md#getalllicencespaged) | **GET** /licences | Get ALL My Licences Paged
*DataOutApi* | [**getLicencesFromDerivedKeys**](docs/Api/DataOutApi.md#getlicencesfromderivedkeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys
*DataOutApi* | [**getVerifiedGtins**](docs/Api/DataOutApi.md#getverifiedgtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
*DataOutApi* | [**getVerifiedGtinsWithLicence**](docs/Api/DataOutApi.md#getverifiedgtinswithlicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)
*DataOutApi* | [**queryBatchById**](docs/Api/DataOutApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*FeedbackApi* | [**queryBatchById**](docs/Api/FeedbackApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*GtinApi* | [**createOrModifyGtins**](docs/Api/GtinApi.md#createormodifygtins) | **POST** /gtins | Create or Modify GTINs - With Licence
*GtinApi* | [**createOrModifyGtinsWithoutLicence**](docs/Api/GtinApi.md#createormodifygtinswithoutlicence) | **POST** /gtins/nolicence | Create or Modify GTINs - Without Licence
*GtinApi* | [**deleteGtins**](docs/Api/GtinApi.md#deletegtins) | **DELETE** /gtins | Delete GTINs
*GtinApi* | [**getVerifiedGtins**](docs/Api/GtinApi.md#getverifiedgtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
*GtinApi* | [**getVerifiedGtinsWithLicence**](docs/Api/GtinApi.md#getverifiedgtinswithlicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)
*GtinApi* | [**queryBatchById**](docs/Api/GtinApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*LicenceApi* | [**createOrUpdateLicences**](docs/Api/LicenceApi.md#createorupdatelicences) | **POST** /licences | Create or Update Licences OR Post Licence No Change Log
*LicenceApi* | [**deleteLicencesByKeyAndType**](docs/Api/LicenceApi.md#deletelicencesbykeyandtype) | **DELETE** /licences | Delete Licences
*LicenceApi* | [**getLicencesFromDerivedKeys**](docs/Api/LicenceApi.md#getlicencesfromderivedkeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys
*LicenceApi* | [**queryBatchById**](docs/Api/LicenceApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*LicencesApi* | [**getAllLicencesPaged**](docs/Api/LicencesApi.md#getalllicencespaged) | **GET** /licences | Get ALL My Licences Paged
*PrefixApi* | [**createPrefixDelegates**](docs/Api/PrefixApi.md#createprefixdelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
*PrefixApi* | [**deleteDelegatedPrefixesByKeyAndType**](docs/Api/PrefixApi.md#deletedelegatedprefixesbykeyandtype) | **DELETE** /prefix/delegates | Delete Delegated Prefixes
*PrefixApi* | [**getAllDelegatedPrefixesPaged**](docs/Api/PrefixApi.md#getalldelegatedprefixespaged) | **GET** /prefix/delegates | Get All Assigned Delegated Prefixes Paged
*PrefixApi* | [**queryBatchById**](docs/Api/PrefixApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*RoleGRPv3DelegateInApi* | [**createPrefixDelegates**](docs/Api/RoleGRPv3DelegateInApi.md#createprefixdelegates) | **POST** /prefix/delegates | Create and assign Delegated Prefixes
*RoleGRPv3DelegateInApi* | [**queryBatchById**](docs/Api/RoleGRPv3DelegateInApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*RoleGRPv3GtinInApi* | [**createOrModifyGtins**](docs/Api/RoleGRPv3GtinInApi.md#createormodifygtins) | **POST** /gtins | Create or Modify GTINs - With Licence
*RoleGRPv3GtinInApi* | [**createOrModifyGtinsWithoutLicence**](docs/Api/RoleGRPv3GtinInApi.md#createormodifygtinswithoutlicence) | **POST** /gtins/nolicence | Create or Modify GTINs - Without Licence
*RoleGRPv3GtinInApi* | [**deleteGtins**](docs/Api/RoleGRPv3GtinInApi.md#deletegtins) | **DELETE** /gtins | Delete GTINs
*RoleGRPv3GtinInApi* | [**queryBatchById**](docs/Api/RoleGRPv3GtinInApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*RoleGRPv3GtinOutApi* | [**getVerifiedGtins**](docs/Api/RoleGRPv3GtinOutApi.md#getverifiedgtins) | **POST** /gtins/verified | Get Verified GTINs (Public)
*RoleGRPv3GtinOutApi* | [**getVerifiedGtinsWithLicence**](docs/Api/RoleGRPv3GtinOutApi.md#getverifiedgtinswithlicence) | **POST** /gtins/verified/licence | Get Verified GTINs With Licence (Private)
*RoleGRPv3LicenceInApi* | [**createOrUpdateLicences**](docs/Api/RoleGRPv3LicenceInApi.md#createorupdatelicences) | **POST** /licences | Create or Update Licences OR Post Licence No Change Log
*RoleGRPv3LicenceInApi* | [**deleteDelegatedPrefixesByKeyAndType**](docs/Api/RoleGRPv3LicenceInApi.md#deletedelegatedprefixesbykeyandtype) | **DELETE** /prefix/delegates | Delete Delegated Prefixes
*RoleGRPv3LicenceInApi* | [**deleteLicencesByKeyAndType**](docs/Api/RoleGRPv3LicenceInApi.md#deletelicencesbykeyandtype) | **DELETE** /licences | Delete Licences
*RoleGRPv3LicenceInApi* | [**queryBatchById**](docs/Api/RoleGRPv3LicenceInApi.md#querybatchbyid) | **GET** /feedback/{batchID} | Query batch status and response
*RoleGRPv3LicenceInfoApi* | [**getLicencesFromDerivedKeys**](docs/Api/RoleGRPv3LicenceInfoApi.md#getlicencesfromderivedkeys) | **POST** /licences/{keyType} | Get Licences from derived GS1 Keys
*RoleGRPv3LicenceOutApi* | [**getAllDelegatedPrefixesPaged**](docs/Api/RoleGRPv3LicenceOutApi.md#getalldelegatedprefixespaged) | **GET** /prefix/delegates | Get All Assigned Delegated Prefixes Paged
*RoleGRPv3LicenceOutApi* | [**getAllLicencesPaged**](docs/Api/RoleGRPv3LicenceOutApi.md#getalllicencespaged) | **GET** /licences | Get ALL My Licences Paged

## Models

- [CountryOfSaleCode](docs/Model/CountryOfSaleCode.md)
- [DelegatedPrefix](docs/Model/DelegatedPrefix.md)
- [DelegatedPrefixAllOf](docs/Model/DelegatedPrefixAllOf.md)
- [DelegatedPrefixAndType](docs/Model/DelegatedPrefixAndType.md)
- [DelegatedPrefixDataOut](docs/Model/DelegatedPrefixDataOut.md)
- [DelegatedPrefixDataOutAllOf](docs/Model/DelegatedPrefixDataOutAllOf.md)
- [ErrorCode](docs/Model/ErrorCode.md)
- [ErrorResponse](docs/Model/ErrorResponse.md)
- [FeedbackErrorResponse](docs/Model/FeedbackErrorResponse.md)
- [FeedbackErrorResponseAllOf](docs/Model/FeedbackErrorResponseAllOf.md)
- [FeedbackResponse](docs/Model/FeedbackResponse.md)
- [FeedbackSuccessResponse](docs/Model/FeedbackSuccessResponse.md)
- [GTIN](docs/Model/GTIN.md)
- [Gtin](docs/Model/Gtin.md)
- [GtinDataIn](docs/Model/GtinDataIn.md)
- [GtinDataInAllOf](docs/Model/GtinDataInAllOf.md)
- [GtinDataOut](docs/Model/GtinDataOut.md)
- [GtinDataOutAllOf](docs/Model/GtinDataOutAllOf.md)
- [GtinValid](docs/Model/GtinValid.md)
- [GtinValidAllOf](docs/Model/GtinValidAllOf.md)
- [GtinVerified](docs/Model/GtinVerified.md)
- [GtinVerifiedWithLicence](docs/Model/GtinVerifiedWithLicence.md)
- [GtinVerifiedWithLicenceAllOf](docs/Model/GtinVerifiedWithLicenceAllOf.md)
- [GtinWithLicenceKeyAndType](docs/Model/GtinWithLicenceKeyAndType.md)
- [KeyType](docs/Model/KeyType.md)
- [LanguageBasedAttribute](docs/Model/LanguageBasedAttribute.md)
- [Licence](docs/Model/Licence.md)
- [LicenceAllOf](docs/Model/LicenceAllOf.md)
- [LicenceDataOut](docs/Model/LicenceDataOut.md)
- [LicenceDataOutAllOf](docs/Model/LicenceDataOutAllOf.md)
- [LicenceDerivedKey](docs/Model/LicenceDerivedKey.md)
- [LicenceDerivedKeyOneOf](docs/Model/LicenceDerivedKeyOneOf.md)
- [LicenceDerivedKeyOneOf1](docs/Model/LicenceDerivedKeyOneOf1.md)
- [LicenceDerivedKeyOneOf2](docs/Model/LicenceDerivedKeyOneOf2.md)
- [LicenceDerivedKeyOneOf3](docs/Model/LicenceDerivedKeyOneOf3.md)
- [LicenceInfo](docs/Model/LicenceInfo.md)
- [LicenceKeyAndType](docs/Model/LicenceKeyAndType.md)
- [LicenceMO](docs/Model/LicenceMO.md)
- [LicenceType](docs/Model/LicenceType.md)
- [LicenseeName](docs/Model/LicenseeName.md)
- [Link](docs/Model/Link.md)
- [Linkset](docs/Model/Linkset.md)
- [LinksetRef](docs/Model/LinksetRef.md)
- [MO](docs/Model/MO.md)
- [NetContent](docs/Model/NetContent.md)
- [PageLink](docs/Model/PageLink.md)
- [PageLinks](docs/Model/PageLinks.md)
- [PageMetadata](docs/Model/PageMetadata.md)
- [PagedDelegatedPrefixes](docs/Model/PagedDelegatedPrefixes.md)
- [PagedDelegatedPrefixesEmbedded](docs/Model/PagedDelegatedPrefixesEmbedded.md)
- [PagedLicences](docs/Model/PagedLicences.md)
- [PagedLicencesEmbedded](docs/Model/PagedLicencesEmbedded.md)
- [Status](docs/Model/Status.md)
- [StatusCode](docs/Model/StatusCode.md)
- [ValidationError](docs/Model/ValidationError.md)

## Authorization

### apiKeyHeader

- **Type**: API key
- **API key parameter name**: APIKey
- **Location**: HTTP header



### apiKeyQuery

- **Type**: API key
- **API key parameter name**: APIKey
- **Location**: URL query string


## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author



## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `v3`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
