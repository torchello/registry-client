<?php
/**
 * GtinApiTest
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * GS1 Registry Platform API
 *
 * This is the high performance API that should only be used for production data coming from your internal systems and Activate-Grade tools
 *
 * The version of the OpenAPI document: v3
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Please update the test case below to test the endpoint.
 */

namespace OpenAPI\Client\Test\Api;

use \OpenAPI\Client\Configuration;
use \OpenAPI\Client\ApiException;
use \OpenAPI\Client\ObjectSerializer;
use PHPUnit\Framework\TestCase;

/**
 * GtinApiTest Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class GtinApiTest extends TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass(): void
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp(): void
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown(): void
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass(): void
    {
    }

    /**
     * Test case for createOrModifyGtins
     *
     * Create or Modify GTINs - With Licence.
     *
     */
    public function testCreateOrModifyGtins()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for createOrModifyGtinsWithoutLicence
     *
     * Create or Modify GTINs - Without Licence.
     *
     */
    public function testCreateOrModifyGtinsWithoutLicence()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for deleteGtins
     *
     * Delete GTINs.
     *
     */
    public function testDeleteGtins()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getVerifiedGtins
     *
     * Get Verified GTINs (Public).
     *
     */
    public function testGetVerifiedGtins()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for getVerifiedGtinsWithLicence
     *
     * Get Verified GTINs With Licence (Private).
     *
     */
    public function testGetVerifiedGtinsWithLicence()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }

    /**
     * Test case for queryBatchById
     *
     * Query batch status and response.
     *
     */
    public function testQueryBatchById()
    {
        // TODO: implement
        $this->markTestIncomplete('Not implemented');
    }
}
