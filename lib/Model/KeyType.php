<?php
/**
 * KeyType
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * GS1 Registry Platform API
 *
 * This is the high performance API that should only be used for production data coming from your internal systems and Activate-Grade tools
 *
 * The version of the OpenAPI document: v3
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;
use \OpenAPI\Client\ObjectSerializer;

/**
 * KeyType Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */
class KeyType
{
    /**
     * Possible values of this enum
     */
    const GCP = 'GCP';

    const GLN = 'GLN';

    const GTIN = 'GTIN';

    const GIAI = 'GIAI';

    /**
     * Gets allowable values of the enum
     * @return string[]
     */
    public static function getAllowableEnumValues()
    {
        return [
            self::GCP,
            self::GLN,
            self::GTIN,
            self::GIAI
        ];
    }
}


