<?php
/**
 * LicenceDerivedKey
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * GS1 Registry Platform API
 *
 * This is the high performance API that should only be used for production data coming from your internal systems and Activate-Grade tools
 *
 * The version of the OpenAPI document: v3
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace OpenAPI\Client\Model;

use \ArrayAccess;
use \OpenAPI\Client\ObjectSerializer;

/**
 * LicenceDerivedKey Class Doc Comment
 *
 * @category Class
 * @package  OpenAPI\Client
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class LicenceDerivedKey implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'LicenceDerivedKey';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'gtin' => '\OpenAPI\Client\Model\GTIN',
        'gcp' => 'string',
        'gln' => 'string',
        'giai' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'gtin' => null,
        'gcp' => null,
        'gln' => null,
        'giai' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'gtin' => 'gtin',
        'gcp' => 'gcp',
        'gln' => 'gln',
        'giai' => 'giai'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'gtin' => 'setGtin',
        'gcp' => 'setGcp',
        'gln' => 'setGln',
        'giai' => 'setGiai'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'gtin' => 'getGtin',
        'gcp' => 'getGcp',
        'gln' => 'getGln',
        'giai' => 'getGiai'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['gtin'] = $data['gtin'] ?? null;
        $this->container['gcp'] = $data['gcp'] ?? null;
        $this->container['gln'] = $data['gln'] ?? null;
        $this->container['giai'] = $data['giai'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['gtin'] === null) {
            $invalidProperties[] = "'gtin' can't be null";
        }
        if ($this->container['gcp'] === null) {
            $invalidProperties[] = "'gcp' can't be null";
        }
        if ((mb_strlen($this->container['gcp']) > 12)) {
            $invalidProperties[] = "invalid value for 'gcp', the character length must be smaller than or equal to 12.";
        }

        if ((mb_strlen($this->container['gcp']) < 4)) {
            $invalidProperties[] = "invalid value for 'gcp', the character length must be bigger than or equal to 4.";
        }

        if (!preg_match("/^\\d{4,12}$/", $this->container['gcp'])) {
            $invalidProperties[] = "invalid value for 'gcp', must be conform to the pattern /^\\d{4,12}$/.";
        }

        if ($this->container['gln'] === null) {
            $invalidProperties[] = "'gln' can't be null";
        }
        if ((mb_strlen($this->container['gln']) > 13)) {
            $invalidProperties[] = "invalid value for 'gln', the character length must be smaller than or equal to 13.";
        }

        if ((mb_strlen($this->container['gln']) < 13)) {
            $invalidProperties[] = "invalid value for 'gln', the character length must be bigger than or equal to 13.";
        }

        if (!preg_match("/^\\d{13}$/", $this->container['gln'])) {
            $invalidProperties[] = "invalid value for 'gln', must be conform to the pattern /^\\d{13}$/.";
        }

        if ($this->container['giai'] === null) {
            $invalidProperties[] = "'giai' can't be null";
        }
        if ((mb_strlen($this->container['giai']) > 30)) {
            $invalidProperties[] = "invalid value for 'giai', the character length must be smaller than or equal to 30.";
        }

        if ((mb_strlen($this->container['giai']) < 5)) {
            $invalidProperties[] = "invalid value for 'giai', the character length must be bigger than or equal to 5.";
        }

        if (!preg_match("/^\\d{4}[A-Za-z0-9\/\\(\\)\\?\\-\"':;<>+=*,.!%&]{1,26}$/", $this->container['giai'])) {
            $invalidProperties[] = "invalid value for 'giai', must be conform to the pattern /^\\d{4}[A-Za-z0-9\/\\(\\)\\?\\-\"':;<>+=*,.!%&]{1,26}$/.";
        }

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets gtin
     *
     * @return \OpenAPI\Client\Model\GTIN
     */
    public function getGtin()
    {
        return $this->container['gtin'];
    }

    /**
     * Sets gtin
     *
     * @param \OpenAPI\Client\Model\GTIN $gtin gtin
     *
     * @return self
     */
    public function setGtin($gtin)
    {
        $this->container['gtin'] = $gtin;

        return $this;
    }

    /**
     * Gets gcp
     *
     * @return string
     */
    public function getGcp()
    {
        return $this->container['gcp'];
    }

    /**
     * Sets gcp
     *
     * @param string $gcp gcp
     *
     * @return self
     */
    public function setGcp($gcp)
    {
        if ((mb_strlen($gcp) > 12)) {
            throw new \InvalidArgumentException('invalid length for $gcp when calling LicenceDerivedKey., must be smaller than or equal to 12.');
        }
        if ((mb_strlen($gcp) < 4)) {
            throw new \InvalidArgumentException('invalid length for $gcp when calling LicenceDerivedKey., must be bigger than or equal to 4.');
        }
        if ((!preg_match("/^\\d{4,12}$/", $gcp))) {
            throw new \InvalidArgumentException("invalid value for $gcp when calling LicenceDerivedKey., must conform to the pattern /^\\d{4,12}$/.");
        }

        $this->container['gcp'] = $gcp;

        return $this;
    }

    /**
     * Gets gln
     *
     * @return string
     */
    public function getGln()
    {
        return $this->container['gln'];
    }

    /**
     * Sets gln
     *
     * @param string $gln gln
     *
     * @return self
     */
    public function setGln($gln)
    {
        if ((mb_strlen($gln) > 13)) {
            throw new \InvalidArgumentException('invalid length for $gln when calling LicenceDerivedKey., must be smaller than or equal to 13.');
        }
        if ((mb_strlen($gln) < 13)) {
            throw new \InvalidArgumentException('invalid length for $gln when calling LicenceDerivedKey., must be bigger than or equal to 13.');
        }
        if ((!preg_match("/^\\d{13}$/", $gln))) {
            throw new \InvalidArgumentException("invalid value for $gln when calling LicenceDerivedKey., must conform to the pattern /^\\d{13}$/.");
        }

        $this->container['gln'] = $gln;

        return $this;
    }

    /**
     * Gets giai
     *
     * @return string
     */
    public function getGiai()
    {
        return $this->container['giai'];
    }

    /**
     * Sets giai
     *
     * @param string $giai giai
     *
     * @return self
     */
    public function setGiai($giai)
    {
        if ((mb_strlen($giai) > 30)) {
            throw new \InvalidArgumentException('invalid length for $giai when calling LicenceDerivedKey., must be smaller than or equal to 30.');
        }
        if ((mb_strlen($giai) < 5)) {
            throw new \InvalidArgumentException('invalid length for $giai when calling LicenceDerivedKey., must be bigger than or equal to 5.');
        }
        if ((!preg_match("/^\\d{4}[A-Za-z0-9\/\\(\\)\\?\\-\"':;<>+=*,.!%&]{1,26}$/", $giai))) {
            throw new \InvalidArgumentException("invalid value for $giai when calling LicenceDerivedKey., must conform to the pattern /^\\d{4}[A-Za-z0-9\/\\(\\)\\?\\-\"':;<>+=*,.!%&]{1,26}$/.");
        }

        $this->container['giai'] = $giai;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


